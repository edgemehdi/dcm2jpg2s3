#!/bin/bash
datetime=`date +%d-%m-%Yat_%H%M`
dicom_source=/home/edgedicom/dicom_files/
dicom_destination=/home/edgedicom/dicom_files_toJPG/
bucket_name="dicom"


#Dicom orig files backup folder
dicom_source_backup=/home/edgedicom/dicom_files_backup

#Folder stores list of filenames at datetime
latest_filenames=/home/edgedicom/latest_filenames/

# Find the filenames  which are created in last 60 mins
find $dicom_source -type f -cmin -60 >  $latest_filename/$datetime.list

#Copy the list of files to dicom_source_backup folder
mkdir -p $dicom_source_backup/$datetime
xargs -a $latest_filename/$datetime.list cp -t $dicom_source_backup/$datetime

for img in $dicom_source_backup/$datetime/*
do
  convert $img $dicom_destination/$img.jpg
done

s3cmd sync $dicom_destination/ s3://$bucket_name/images/




#####If u want to rename long name files to shortnames
#n=0;
#for i in $dicom_source_backup/$datetime/1.3.6.1.4.1.11157.164784921115781.1463166523.727793*
#do
#  mv "$i" "Ultrasound-$n${i#1.3.6.1.4.1.11157.164784921115781.1463166523.727793*}"
#done

