# Instructions to set up AWS s3 configuration on Ubuntu.
suppose my server username is  edgedicom

- sudo apt-get install s3cmd.
- s3cmd --configure.
- Enter Aws access_key & Secret_key.  
- it will prompt for `passwd` to be set (optional) or you can just hit enter for blank
- prompt for gpg path just hit enter.
- type Yes for https communication.
- save settings type y.
- settings will be saved in */home/edgedicom/.s3cfg*
## Test aws connectivity to S3.
suppose you have bucket name dicom.
* s3cmd ls s3://dicom/.

# For converting Dicom images to JPG and push to Aws-S3  using bash script.
1. Install Imagemagick. 
 `sudo apt-get install imagemagick`

2. Set a period in crontab  when you want to run the script (dcm2jpg2s3.sh) based on that in the script you can change cmin time in script.
ex:-  i have set it for 60mins.

3. How script works.

- list of  filenames created  within 60 minutes in dicom_source folder  will be stored in file $latest_filename/$datetime.list. 
- Only list of files will be copied to folder $dicom_source_backup/$datetime.
- Using command  convert  dcm files  from $dicom_source_backup/$datetime  to the destination folder $dicom_destination.
- Destination folder will be synced with s3bucket.